import java.nio.file.Path
import java.nio.file.Paths
import groovy.json.JsonOutput

/*
    This class indexes all files in a BIDS dataset and stores them in a list of maps.
    For each file, the following properites are stored:
    - `path`   - the absolute path to the file
    - `sub`    - the subject label
    - `ses`    - the session label
    - `suffix` - the BIDS suffix of the file
    - `name`   - the basename of the file
    - `ext`    - the file extention (suffix after the first dot in `name`)
    - `echo`   - the echo number for multi-echo scan
 */
class BIDSIndex {
    /*
        Path to input BIDS directory
     */
    private Path bidsDir
    /*
        List of maps, where each map represents one BIDS file
     */
    private List allFiles

    /*
        Constructor with path string to input BIDS directory
     */
    BIDSIndex(String bidsDir) {
        this.bidsDir = Paths.get(bidsDir).toAbsolutePath()
        this.allFiles = indexAllFiles()
    }

    /*
        Recursively finds all files in the BIDS input directory.
        For each file multiple pattern matchings are executed to retrieve values of BIDS entities.
        Each BIDS entity is stored as a "key" : "value" pair.
        Currently retrieved entities are summarized in the class description.
     */
    private List indexAllFiles() {
        def allFiles = []
        this.bidsDir.eachFileRecurse(groovy.io.FileType.FILES) {

            def f = [:]

            // File path
            f.put("path", it.toString())

            // Subject
            def matcher = it.toString() =~ /.*\/sub-(.+?)\/.*/
            if (matcher.size() > 0) {
                f.put("sub", matcher[0][1])
            } else {
                f.put("sub", "null")
            }

            // Session
            matcher = it.toString() =~ /.*\/ses-(.+?)\/.*/
            if (matcher.size() > 0) {
                f.put("ses", matcher[0][1])
            } else {
                f.put("ses", "null")
            }

            // Suffix
            matcher = it.toString() =~ /.*_(.+?)\..*/
            if (matcher.size() > 0) {
                f.put("suffix", matcher[0][1])
            } else {
                f.put("suffix", "null")
            }

            // File name
            f.put("name", it.getFileName().toString())

            // File extension
            def fileName = it.getFileName().toString()
            def dotIndex = fileName.indexOf('.')
            if (dotIndex > 0) {
                f.put("ext", fileName.substring(dotIndex + 1))
            } else {
                f.put("ext", "null")
            }

            // Echo
            matcher = it.toString() =~ /.*_echo-(.+?)_.*/
            if (matcher.size() > 0) {
                f.put("echo", matcher[0][1])
            } else {
                f.put("echo", "null")
            }


            allFiles.add(f)
        }
        return allFiles
    }

    /*
        Returns the list of all indexed BIDS files.
     */
    public List getAllFiles() {
        return this.allFiles
    }

    /*
        Pretty-prints all the BIDS files to the standard output.
     */
    public void printAllFiles() {
        println JsonOutput.prettyPrint(JsonOutput.toJson(this.allFiles))
    }

}
