// Importpipeline parameter validations and BIDS indexing
include { bidsInputFilter } from './bids_input_filter.nf'

process FSLBet {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/containers/fsl-bet-flirt:latest'

    input:
        val bidsInput
    
    output:
        tuple val(bidsInput), path('fsl_bet_out.nii')
    
    script:
        optionR = params.fslBetR ? "-R" : ""
        """
        echo "FSL Bet for input ${bidsInput.path}"
        bet "${bidsInput.path}" "fsl_bet_out.nii" -f ${params.fslBetf} ${optionR} -v
        """
}

process FSLFlirt {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/containers/fsl-bet-flirt:latest'

    input:
        tuple val(bidsInput), path(fsl_bet)
    
    output:
        tuple val(bidsInput), path('fsl_flirt_out.nii')
    
    """
    echo "FSL Flirt for input ${bidsInput.path}"
    flirt -in "${fsl_bet}" -ref "${baseDir}/data/MNI152_T1_1mm_brain_LPS_filled.nii.gz" -out "fsl_flirt_out.nii"
    """
}

process DeepBrainNetAge {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/containers/deepbrainnet:latest'

    input:
        tuple val(bidsInput), path(fsl_flirt)
    
    output:
        tuple val(bidsInput.sub), path('t1_age.txt')
    
    """
    echo "DeepBrainNet"

    NF_PROCESS_WORKDIR=\${PWD}

    . "/opt/miniconda-latest/etc/profile.d/conda.sh"
    cd /opt/DeepBrainNet/Script
    conda activate DeepBrainNet
    
    mkdir -p \${NF_PROCESS_WORKDIR}/tmp/Test/
    mkdir -p \${NF_PROCESS_WORKDIR}/output
    mkdir -p \${NF_PROCESS_WORKDIR}/input
    cp \${NF_PROCESS_WORKDIR}/${fsl_flirt} \${NF_PROCESS_WORKDIR}/input/S001_S1_T1_robust.nii
    
    python Slicer.py \${NF_PROCESS_WORKDIR}/input/ \${NF_PROCESS_WORKDIR}/tmp/
    python Model_Test.py \${NF_PROCESS_WORKDIR}/tmp/ \${NF_PROCESS_WORKDIR}/output/pred.csv ../Models/DBN_model.h5
    
    conda deactivate
    cat \${NF_PROCESS_WORKDIR}/output/pred.csv | awk -v FS=, 'NR==2 {printf \$2}' > \${NF_PROCESS_WORKDIR}/t1_age.txt
    """
}

// FSL-based preprocessing subworkflow
workflow FSLPreprocessing {
    take:
        bidsInputChannel
    
    main:
        FSLBet(bidsInputChannel)
            | FSLFlirt
    
    emit:
        FSLFlirt.out
}

process ANTsPyNet {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/containers/antspynet:latest'

    input:
        tuple val(bidsInput), path(fsl_flirt)
    
    output:
        tuple val(bidsInput.sub), path('t1_age.txt')
    
    """
    antspynet_brainage.py ${fsl_flirt} --antsxnet-cache ${params.antspynetCacheDir}
    """
}

process ANTsPyNetWithPreprocessing {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/containers/antspynet:latest'

    input:
        val bidsInput
    
    output:
        tuple val(bidsInput.sub), path('t1_age.txt')
    
    """
    antspynet_brainage.py ${bidsInput.path} --do-preprocessing --antsxnet-cache ${params.antspynetCacheDir}
    """
}

workflow {
    // Prepare the derivative
    // Copy all files from `./assets` to derivative dir
    file(params.derivativeDir).mkdirs()
    file("${workflow.projectDir}/assets/").eachFile {
        it.copyTo(params.derivativeDir)
    }

    // Get a channel with all BIDS input files that survived filtering based on command line parameters.
    bf = bidsInputFilter()

    // Filter out all T1w nii files
    bf.filter { it.suffix == 'T1w' && (it.ext == 'nii' || it.ext == 'nii.gz') && !(it.path ==~ /.*derivatives.*/) }
        | (ANTsPyNetWithPreprocessing & FSLPreprocessing) // Send all T1w to two preprocessing processes simultanously
    
    FSLPreprocessing.out                                  // Take the output of FSL-based preprocessing
        | (ANTsPyNet & DeepBrainNetAge)                   // Send it to brain age estimating processes that require preprocessing
        | join                                            // Join the brain age estimation results (with FSL preprocessing) on the first tuple element
        | join(ANTsPyNetWithPreprocessing.out)            // Join with the results with ANTs preprocessing on the first tuple element
        | toSortedList( { a, b -> a[0] <=> b[0] } )       // Sort the joined results
        | flatMap                                         // `toSortedList` outputs one list - unpack it
        | subscribe {                                     // Reformat each item and append to the results file
            file("${params.derivativeDir}/participants.tsv").append("sub-${it[0]}\t${it[1].text}\t${it[2].text}\t${it[3].text}\n")
        }

}
