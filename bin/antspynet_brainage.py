#!/usr/bin/env python3

import argparse
import ants
import antspynet

parser = argparse.ArgumentParser(description='Execute ANTsPyNet-based brain age estimation for one image and save results into a file.')
parser.add_argument(
    'input_image',
    help = 'Input image file'
)
parser.add_argument(
    '--do-preprocessing',
    action='store_true',
    help = 'Boolean specifying if ANTsPyNet preprocessing should be executed'
)
parser.add_argument(
    '--antsxnet-cache',
    help = 'Cache directory for ANTsPyNet'
)
parser.add_argument(
    '--output-file',
    help = 'File to save the result',
    default='t1_age.txt'
)
args = parser.parse_args()

t1 = ants.image_read(args.input_image)
deep = antspynet.brain_age(
    t1,
    number_of_simulations=0,
    sd_affine=0.01,
    antsxnet_cache_directory=args.antsxnet_cache,
    do_preprocessing=args.do_preprocessing,
    verbose=True
)

# Keras prints downloading info to stdout, which cannot be supressed
# Additionally, we may want to look at the verbose outputs
# Write the age into a file
with open(args.output_file, 'w') as out:
    out.write(str(deep['predicted_age']))
