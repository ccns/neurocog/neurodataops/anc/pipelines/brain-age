/*
 Pipeline parameters validation and parsing
 */

// Import nf-validation plugin
include { validateParameters; paramsHelp; paramsSummaryLog } from 'plugin/nf-validation'
// Print help message, supply typical command line usage for the pipeline
if (params.help) {
   log.info paramsHelp("nextflow main.nf --bids /absolute/path/to/my/bids/dataset")
   exit 0
}
// Validate input parameters
validateParameters()
// Print summary of supplied parameters
log.info paramsSummaryLog(workflow)

// List of subjects if provided by user
subList = null
if (params.sub) {
    subList = params.sub.toString().tokenize(',')
}
// List of sessions if provided by user
sesList = null
if (params.ses) {
    sesList = params.ses.toString().tokenize(',')
}

/*
 Indexinfg all BIDS files and initial filtering of subjects and sessions
 */

bidsIndex = new BIDSIndex(params.bids)

// This workflow emits a channel with all BIDS files from BIDSIndex that survive input filtering
workflow bidsInputFilter {
    main:
        // Get a channel with a list of all BIDS files as documented in BIDSIndex
        bf = Channel.fromList(bidsIndex.getAllFiles())
        // Filer for subjects
        if (subList != null) {
            bf = bf.filter {subList.contains(it.sub)}
        }
        // Filter for session
        if (sesList != null) {
            bf = bf.filter {sesList.contains(it.ses)}
        }

    emit:
        bf
}
