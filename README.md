# Brain Age

This [Nextflow](https://www.nextflow.io/) pipeline estimates brain age using the original code [DeepBrainNet](https://github.com/vishnubashyam/DeepBrainNet) and its re-implementation from [ANTsPyNet](https://antsx.github.io/ANTsPyNet/docs/build/html/utilities.html#antspynet.utilities.brain_age). More information and extended documentation can be also found in the [original project's fork](https://github.com/tannerjared/DeepBrainNet).

## Dependencies

The pipeline is implemented with [Nextflow](https://www.nextflow.io/) and [nf-validation](https://nextflow-io.github.io/nf-validation/). You need Nextflow to execute it. We resort to [Nextflow's documentation](https://www.nextflow.io/docs/latest/index.html) for all Nextflow-relevant issues.

### Containers

The pipeline processes are executed in containers, which deliver the respective software. We implemented our own Dockerfiles and host the Docker images in GitLab container registries. The following table summarizes the Docker images.

| Process | Dockerfile | Image |
| :------ | :--------- | :-------- |
| FSL Bet/Flirt | [URL](https://gitlab.com/ccns/neurocog/neurodataops/anc/containers/fsl-bet-flirt) | [URL](https://gitlab.com/ccns/neurocog/neurodataops/anc/containers/fsl-bet-flirt/container_registry/6011771) |
| DeepBrainNet | [URL](https://gitlab.com/ccns/neurocog/neurodataops/anc/containers/deepbrainnet) | [URL](https://gitlab.com/ccns/neurocog/neurodataops/anc/containers/deepbrainnet/container_registry/6011931) |
| ANTsPyNet(WithPreprocessing) | [URL](https://gitlab.com/ccns/neurocog/neurodataops/anc/containers/antspynet) | [URL](https://gitlab.com/ccns/neurocog/neurodataops/anc/containers/antspynet/container_registry/6011850) |

### Apptainer is recommended

Currently, the pipeline supports [Apptainer](https://apptainer.org/), [Singularity](https://docs.sylabs.io/guides/main/user-guide/), and [Docker](https://www.docker.com/). One of these systems must be installed on the host machine. When executing the pipeline, choose the right one with the `-profile` option. [`nextflow.config`](./nextflow.config) lists the specific configurations.

Making sure that the pipeline executes correctly with Docker is difficult. Moreover, in our execution environments, we support only Apptainer. Therefore, Apptainer is the recommended container system and we may remove instructions and configurations of other systems.

### Windows notes

On Windows, install [Nextflow](https://www.nextflow.io/docs/latest/getstarted.html) and [Apptainer](https://apptainer.org/docs/admin/1.2/installation.html#install-from-pre-built-packages) with [WSL](https://learn.microsoft.com/en-us/windows/wsl/install). Then, all Linux instructions should work.

## Pipeline visualization

```mermaid
flowchart TB
    subgraph " "
    v0["Channel.fromList"]
    end
    v2([ANTsPyNetWithPreprocessing])
    subgraph FSLPreprocessing
    v3([FSLBet])
    v4([FSLFlirt])
    end
    v5([ANTsPyNet])
    v6([DeepBrainNetAge])
    v1(( ))
    v7(( ))
    v0 --> v1
    v1 --> v2
    v2 --> v7
    v1 --> v3
    v3 --> v4
    v4 --> v5
    v4 --> v6
    v5 --> v7
    v6 --> v7
```

## Input

The input to the pipeline is a [BIDS](https://bids-specification.readthedocs.io/en/stable/) dataset. The pipeline is executed for images with `T1w` suffix in the BIDS dataset. We are working on [Nextflow BIDS Recipes](https://gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/nextflow-bids) that simplify implementing Nextflow pipelines for BIDS datasets. The input can be further filtered for specific subjects and sessions using `--sub` and `--ses` options.

## Preprocessing notes

In the FSL-based preprocessing, the `T1w` images are realigned with FSL Flirt using [the original template](https://upenn.app.box.com/v/DeepBrainNet/file/735993049647) linked on the [DeepBrainNet project website](https://github.com/vishnubashyam/DeepBrainNet). Due to lack of documentation, we are guessing that this is the template, which was originally used for the realignment of the training data.

## Execution

The pipeline can be executed by providing URL of this GitLab repository. Execute the following command to print the help message in the terminal.

The pipeline is under development.

```
nextflow run https://gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/brain-age -revision 1.0.0 --help
```

Provide the path to your BIDS dataset to execute the pipeline.

```
nextflow run https://gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/brain-age -revision 1.0.0 -profile apptainer --bids /absolute/path/to/my/bids/dataset
```

The pipeline can be also executed after cloning this repository.

```
nextflow run main.nf -profile apptainer --bids /absolute/path/to/my/bids/dataset
```

### Options

Follow the instructions from the help message for more options.

### Work directory

The pipeline stores intermediate results in a work directory, by default `<Nextflow launch directory>/work`. Use `-work-dir` Nextflow option to change it.

```
nextflow run main.nf -profile apptainer -work-dir /absolute/path/to/work/dir --bids /absolute/path/to/my/bids/dataset
```

## Output

The pipeline saves the results as a [BIDS derivative](https://bids-specification.readthedocs.io/en/stable/derivatives/introduction.html). For each subject, three age estimations are saved in `participants.tsv` file in default `<bids>/derivatives/brainage-<version>` directory. The columns are described in [`participants.json`](./assets/participants.json) file.

As shown in the [pipeline diagram](#pipeline-visualization), three brain age estimations are executed:

| Estimation implementation | Preprocessing |
| :------------------------ | :------------ |
| [DeepBrainNet](https://github.com/vishnubashyam/DeepBrainNet) | FSL Bet + FSL Flirt|
| [ANTsPyNet](https://antsx.github.io/ANTsPyNet/docs/build/html/utilities.html#antspynet.utilities.brain_age) | FSL Bet + FSL Flirt |
| [ANTsPyNet](https://antsx.github.io/ANTsPyNet/docs/build/html/utilities.html#antspynet.utilities.brain_age) | ANTsPyNet internal |
